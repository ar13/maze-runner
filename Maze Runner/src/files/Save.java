package files;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Iterator;

import org.apache.logging.log4j.Logger;

import Model.Item;
import control.MyLogger;

public class Save {
	Originator originator;
	Item[][] items;
	char[][] arrayToSave;
	String fileName;
	private String absoluteName;

	private static final Logger LOGGER = MyLogger.getInstance();

	public Save(Originator originator) {
		LOGGER.info("Saving the game...");
		this.originator = originator;
	}

	public void save(String fileName) {
		this.absoluteName = fileName;
		this.fileName = "./file/save/" + fileName + ".txt";
		createFile();
		items = originator.getState().getItems();
		arrayToSave = new char[items.length][items[0].length];
		convertItemsToChar();
		saveToSavedFiles();
		saveToFile();
		LOGGER.info("Game is saved.");
	}

	private void saveToSavedFiles() {
		int sum;
		try {
			FileReader fr = new FileReader("./file/savedFiles.txt");
			BufferedReader br = new BufferedReader(fr);
			sum = Integer.parseInt(br.readLine());
			ArrayList<String> gameNames = new ArrayList<String>();
			String verify = null;
			while ((verify = br.readLine()) != null) {
				if (verify != null) {
					gameNames.add(verify);
				}
			}
			br.close();

			FileWriter fileWriter;
			fileWriter = new FileWriter("./file/savedFiles.txt");
			BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
			bufferedWriter.write(String.valueOf(sum + 1));
			bufferedWriter.newLine();
			Iterator<String> itr = gameNames.iterator();
			while (itr.hasNext()) {
				bufferedWriter.write(itr.next());
				bufferedWriter.newLine();
			}
			bufferedWriter.write(absoluteName);
			bufferedWriter.close();
		} catch (IOException e) {
			LOGGER.warn("ERROR: Something went wrong while saving game " + fileName + " - line "
					+ Thread.currentThread().getStackTrace()[1].getLineNumber());
		} catch (NumberFormatException e) {
			LOGGER.warn(
					"ERROR: NumberFormatException - line " 
			+ Thread.currentThread().getStackTrace()[1].getLineNumber());
		}
	}

	private void createFile() {
		LOGGER.info("Creating new file...");
		try {
			File file = new File(fileName);
			if (file.createNewFile()) {
				LOGGER.info("File created.");
			} else {
				LOGGER.info("File already exits.");
			}

		} catch (IOException e) {
			LOGGER.warn("ERROR: IOExceptoin - line " + Thread.currentThread().getStackTrace()[1].getLineNumber());
		}
	}

	private void convertItemsToChar() {
		for (int i = 0; i < items.length; i++) {
			for (int j = 0; j < items[0].length; j++) {
				switch (items[i][j].getClass().getSimpleName()) {
				case "BrickWall":
					arrayToSave[i][j] = 'W';
					break;
				case "EmptyCell":
					arrayToSave[i][j] = 'E';
					break;
				case "BulletsGift":
					arrayToSave[i][j] = 'U';
					break;
				case "BigBomb":
					arrayToSave[i][j] = 'O';
					break;
				case "BigSlowMonster":
					arrayToSave[i][j] = 'M';
					break;
				case "HealthGift":
					arrayToSave[i][j] = 'H';
					break;
				case "Player":
					arrayToSave[i][j] = 'P';
					break;
				case "SmallBomb":
					arrayToSave[i][j] = 'o';
					break;
				case "SmallFastMonster":
					arrayToSave[i][j] = 'm';
					break;
				case "WoodWall":
					arrayToSave[i][j] = 'w';
					break;
				case "EndCell":
					arrayToSave[i][j] = 'F';
					break;
				}

			}
		}

	}

	private void saveToFile() {
		try {
			FileWriter fileWriter = new FileWriter(fileName);
			BufferedWriter bufferedWriter = new BufferedWriter(fileWriter);
			bufferedWriter.write("score : " + originator.getState().getScore());
			bufferedWriter.newLine();
			bufferedWriter.write("difficulty : " + originator.getState().getDifficulty());
			bufferedWriter.newLine();
			bufferedWriter.write("health : " + originator.getState().getHealth());
			bufferedWriter.newLine();
			bufferedWriter.write("lives : " + originator.getState().getLives());
			bufferedWriter.newLine();
			bufferedWriter.write("bullets : " + originator.getState().getBullets());
			bufferedWriter.newLine();
			bufferedWriter.write("time : " + originator.getState().getTime());
			bufferedWriter.newLine();

			for (int i = 0; i < arrayToSave.length; i++) {
				for (int j = 0; j < arrayToSave[0].length; j++) {
					bufferedWriter.write(arrayToSave[i][j]);
				}
				bufferedWriter.newLine();
			}

			bufferedWriter.close();
		} catch (IOException ex) {
			System.out.println("Error writing to file '" + fileName + "'");

		}
	}

}
