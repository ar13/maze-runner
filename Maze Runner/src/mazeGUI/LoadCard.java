package mazeGUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Component;
import java.awt.Dimension;
import java.awt.Font;
import java.io.File;
import java.io.FileNotFoundException;
import java.util.Scanner;

import javax.swing.JPanel;
import javax.swing.JScrollBar;
import javax.swing.JScrollPane;
import javax.swing.JTable;
import javax.swing.SwingConstants;
import javax.swing.table.DefaultTableCellRenderer;
import javax.swing.table.DefaultTableModel;
import javax.swing.table.JTableHeader;
import javax.swing.table.TableCellRenderer;
import javax.swing.table.TableModel;

import org.apache.logging.log4j.Logger;

import control.MyLogger;
import mazeGUI.ScoreBoard.MyScrollBarUI;

@SuppressWarnings("serial")
public class LoadCard extends JPanel {

	private Scanner scan;
	private final String filePath = "./file/savedFiles.txt";
	private final String[] columnsNames = { "Order", "Game Name" };
	private String[][] data;
	private JTable table;

	private static final Logger LOGGER = MyLogger.getInstance();

	public LoadCard() {
		LOGGER.info("Initializing the load card...");
		openFile(filePath);
		readFile();
		closeFile();

		initialize();
	}

	private void initialize() {
		setLayout(new BorderLayout());
		DefaultTableModel model = new DefaultTableModel(data, columnsNames) {
			@Override
			public boolean isCellEditable(int row, int column) {
				return false;
			}
		};
		table = setTable(model);
		JScrollPane scrollPane = new JScrollPane(table);
		JScrollBar bar = scrollPane.getVerticalScrollBar();
		bar.setPreferredSize(new Dimension(10, 0));
		scrollPane.getVerticalScrollBar().setUI(new MyScrollBarUI());
		scrollPane.setBackground(Color.BLACK);
		add(scrollPane, BorderLayout.CENTER);
	}

	private void openFile(final String path) {
		try {
			scan = new Scanner(new File(path));
		} catch (FileNotFoundException e) {
			LOGGER.error("ERROR: Couldn't find the saved game! - Line "
					+ Thread.currentThread().getStackTrace()[1].getLineNumber());

		}
	}

	private void readFile() {
		int length = 0;
		try {
			length = Integer.parseInt(scan.nextLine());
		} catch (Exception e) {
			LOGGER.error("ERROR: Couldn't find the saved game! - Line "
					+ Thread.currentThread().getStackTrace()[1].getLineNumber());
		}
		if (length > 0) {
			data = new String[length][columnsNames.length];
			int i = 0;
			while (scan.hasNext()) {
				data[i][0] = String.valueOf(i + 1);
				data[i][1] = scan.nextLine();
				i++;
			}
		}
	}

	private void closeFile() {
		scan.close();
	}

	private JTable setTable(DefaultTableModel model) {
		table = new JTable(model) {
			public Component prepareRenderer(TableCellRenderer renderer, int row, int column) {
				Component c = super.prepareRenderer(renderer, row, column);
				if (!isRowSelected(row)) {
					c.setBackground(row % 2 == 0 ? getBackground() : Color.LIGHT_GRAY);
				}
				return c;
			}
		};
		table.setCellSelectionEnabled(true);
		setCellsAlignment(table, SwingConstants.CENTER);
		JTableHeader th = table.getTableHeader();
		th.setBackground(Color.BLACK);
		th.setForeground(Color.WHITE);
		th.setFont(new Font("Calibri", Font.BOLD, 20));
		table.setBackground(Color.PINK);
		table.setFont(new Font("Calibri", Font.BOLD, 18));
		table.setRowHeight(30);

		// table.setEnabled(false);
		table.setShowHorizontalLines(false);
		table.getSelectionModel().setSelectionInterval(0, 0);
		return table;
	}

	private void setCellsAlignment(JTable table, int alignment) {
		DefaultTableCellRenderer rightRenderer = new DefaultTableCellRenderer();
		rightRenderer.setHorizontalAlignment(alignment);
		TableModel tableModel = table.getModel();
		for (int columnIndex = 0; columnIndex < tableModel.getColumnCount(); columnIndex++) {
			table.getColumnModel().getColumn(columnIndex).setCellRenderer(rightRenderer);
		}
	}

	public String getSelectedFile() {
		final int rowIndex = table.getSelectedRow();
		if (rowIndex < 0) {
			return "";
		}
		return data[rowIndex][1];
	}
}
