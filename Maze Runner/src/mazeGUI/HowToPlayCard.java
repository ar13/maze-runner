package mazeGUI;

import java.awt.BorderLayout;
import java.awt.Color;
import java.awt.Dimension;
import java.awt.Font;
import java.awt.Graphics;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.awt.Image;

import javax.imageio.ImageIO;
import javax.swing.BoxLayout;
import javax.swing.ImageIcon;
import javax.swing.JPanel;
import javax.swing.JTextArea;

import org.apache.logging.log4j.Logger;

import control.MyLogger;


@SuppressWarnings("serial")
public class HowToPlayCard extends JPanel {

	private final String lineSeparator = System.getProperty("line.separator");
	private Image healthGift;
	private Image bulletsGift;
	private Image bomb;
	private Image monster1;
	private Image monster2;
	private Image wall;
	private Image tree;
	private Image fire;
	
	private static final Logger LOGGER = MyLogger.getInstance();
	
	public HowToPlayCard() {
		LOGGER.info("Initializing the HowToPlay card...");
		setBackground(new Color(133, 173, 173));
		BorderLayout borderLayout = new BorderLayout(10, 10);
		setLayout(borderLayout);
		displayUserGuide();
		JPanel buttons = new JPanel();
		addButtons(buttons);
		this.add(buttons, BorderLayout.SOUTH);
	}
	
	public void paint(Graphics g) {
		super.paint(g);
		try {
			drawIcons(g);
		} catch (IOException e) {
			LOGGER.error("ERROR: IOExceptoin - line " 
					+ Thread.currentThread().getStackTrace()[1].getLineNumber());
		}
		repaint();
	}
	
	private void drawIcons(Graphics g) throws IOException {
		ImageIcon img = new ImageIcon("./icon/fixed/gift/health.png");
		healthGift = img.getImage();
		img = new ImageIcon("./icon/fixed/gift/ammo.png");
		bulletsGift = img.getImage();
		img = new ImageIcon("./icon/fixed/bomb/bomb1.png");
		bomb = img.getImage();
		img = new ImageIcon("./icon/wall/wall1.png");
		wall = img.getImage();
		BufferedImage BI = ImageIO.read(new File("./icon/animation/monster/monster0.png"));
		monster1 = BI.getSubimage(0, 0, 31, 32);
		BI = ImageIO.read(new File("./icon/animation/monster/monster1.png"));
		monster2 = BI.getSubimage(0, 0, 31, 32);
		img = new ImageIcon("./icon/tree/box.png");
		tree = img.getImage();
		img = new ImageIcon("./icon/animation/fire/fire0.png");
		fire = img.getImage();
		g.drawImage(healthGift, 280, 88, null);
		g.drawImage(bulletsGift, 320, 88, null);
		g.drawImage(bomb, 505, 88, null);
		g.drawImage(monster1, 680, 88, null);
		g.drawImage(monster2, 720, 88, null);
		g.drawImage(tree, 515, 140, null);
		g.drawImage(wall, 650, 140, null);
		g.drawImage(fire, 605, 190, null);
		g.drawImage(fire, 620, 190, null);
		g.drawImage(fire, 635, 190, null);
		g.drawImage(fire, 605, 210, null);
		g.drawImage(fire, 620, 210, null);
		g.drawImage(fire, 635, 210, null);
	}
	
	private void displayUserGuide() {
		JTextArea userGuide = new JTextArea();
		StringBuilder sb = new StringBuilder();
		sb.append(lineSeparator + "     Use the four arrow keys to run through the maze");
		sb.append(lineSeparator + lineSeparator + "     while running collect gifts");
		sb.append("                  avoid bombs       and monsters");
		sb.append(lineSeparator + lineSeparator + "     In your way out, you will face two obstacles:");
		sb.append(" boxes       and walls");
		sb.append(lineSeparator + lineSeparator + "     You can overcome walls and monsters using your bullets");
		sb.append(lineSeparator + lineSeparator + "     Use the spacebar to shot a bullet of which you can have only six");
		sb.append(lineSeparator + lineSeparator + "     You start with three lifes and the game ends if you reach the exit gate");
		sb.append(lineSeparator + lineSeparator + "     or died while trying.");
		sb.append(lineSeparator + lineSeparator + "     Remember to go the settings and personalize your maze.");
		userGuide.setFont(new Font("Cooper Std Black", Font.PLAIN, 22));
		userGuide.setForeground(new Color(0, 51, 102));
		userGuide.setBackground(null);
		userGuide.setBorder(null);
		userGuide.setEditable(false);
		userGuide.setText(sb.toString());
		this.add(userGuide);
	}
	
	private void addButtons(JPanel buttons) {
		buttons.setPreferredSize(new Dimension(120, 40));
		buttons.setBackground(null);
		buttons.setBorder(null);
		buttons.setLayout(new BoxLayout(buttons, BoxLayout.Y_AXIS));
		/*
		JButton tryGameBtn = new JButton("Try it now!");
		tryGameBtn.setPreferredSize(new Dimension(50, 30));
		tryGameBtn.setBackground(new Color(0, 51, 102));
		tryGameBtn.setForeground(Color.WHITE);
		tryGameBtn.addActionListener(new ActionListener() {
			@Override
			public void actionPerformed(ActionEvent e) {
				//should be short game to clarify features
				Game game = new Game();
				game.gameplay = new MazeGUI();
				game.initialize();
				game.switchCard("gameplay");
				game.back.setVisible(true);
			}

		});
		buttons.add(tryGameBtn);
		*/
	}
}
