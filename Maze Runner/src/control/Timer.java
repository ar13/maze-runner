package control;

import org.apache.logging.log4j.Logger;

public class Timer implements Runnable{
	
	private int seconds = 0;
	private int minutes = 0;
	/*
	private final int DURATION = 180;
	private int elapsedTime = 0;
	*/
	private String time;
	
	private static final Logger LOGGER = MyLogger.getInstance();
	
	@Override
	public void run() {
		while (true) {
			if (seconds == 60) {
				seconds = 0;
				minutes++;
			}
			time = String.format("%02d", minutes) + ":" + String.format("%02d", seconds);
			Controller.updateTime(time);
			try {
				Thread.sleep(1000);
			} catch(InterruptedException ie) {
				LOGGER.warn("ERROR: Couldn't cease execution of time thread - line " 
						+ Thread.currentThread().getStackTrace()[1].getLineNumber());
			}
			seconds++;
			//elapsedTime++;
		}
	}
	
	public String getTime() {
		return time;
	}
	
	public void setSeconds(final int seconds) {
		this.seconds = seconds;
	}
	
	public void setMinutes(final int minutes) {
		this.minutes = minutes;
	}
	
	public void resetTime() {
		seconds = 0;
		minutes = 0;
		//elapsedTime = 0;
	}
}
