package control;

import org.apache.logging.log4j.LogManager;
import org.apache.logging.log4j.Logger;

public class MyLogger {

	/**
	 * 
	 */
	private static Logger logger = null;

	/**
	 * 
	 */
	private MyLogger() {
	}
	
	/**
	 * @return logger.
	 */
	public static Logger getInstance() {
		if (logger == null) {
			logger = LogManager.getLogger(MyLogger.class.getName());
		}
		return logger;
	}
}
