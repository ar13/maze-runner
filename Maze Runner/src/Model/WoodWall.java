package Model;

import java.awt.Image;

import javax.swing.ImageIcon;

public class WoodWall extends Wall{
	
	//private final Icon woodWallIcon = new ImageIcon("");
	
	private Image icon = new ImageIcon("./icon/tree/box.png").getImage().getScaledInstance(18, 18, Image.SCALE_DEFAULT);
	
	public WoodWall(){
		setDestructable(true);
		//setIcon(woodWallIcon);
	}

	@Override
	public void setIcon(Image newIcon) {
		icon = newIcon;
	}

	@Override
	public Image getIcon() {
		return icon;
	}
}
