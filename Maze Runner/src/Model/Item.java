package Model;

import java.awt.Image;
import java.awt.Point;

public abstract class Item {

	private Point position;

	public Point getPoistion() {
		return position;
	}

	public void setPosition(Point newPosition) {
		position = newPosition;
	}

	public abstract void setIcon(Image newIcon);

	public abstract Image getIcon();

}
