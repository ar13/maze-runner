package Model;

import java.awt.Image;
import java.awt.image.BufferedImage;
import java.io.File;
import java.io.IOException;
import java.util.Iterator;

import javax.imageio.ImageIO;
import javax.swing.ImageIcon;

import control.Direction;

public class Player extends Item {
	private static Image icon = new ImageIcon().getImage();
	private static BufferedImage sprite;

	private static Player firstInstance = null;

	private int health;
	private Gun gun;
	private int lives;
	private int score = 0;

	private Collides collides;

	private final int width = 31;
	private final int height = 32;
	private int downCounter = 0; // to 2
	private int rightCounter = 6; // to 8
	private int upCounter = 9; // to 11
	private int leftCounter = 3; // to 5
	private static BufferedImage[] sprites;

	private Player() {
		init();
	}

	public void init() {
		health = 100;
		gun = new Gun();
		lives = 3;
		collides = new PlayerCollide();
		setSprite("./icon/animation/runner/runner0.png");
	}

	public Gun getGun() {
		return gun;
	}

	public void setGun(Gun gun) {
		this.gun = gun;
	}

	public int getHealth() {
		return health;
	}

	public void setHealth(int health) {
		this.health = health;
	}

	public int getLives() {
		return lives;
	}

	public void setLives(int lives) {
		this.lives = lives;
	}

	public int getScore() {
		return score;
	}

	public void addScore(int scoreAdded) {
		score += scoreAdded;
	}

	public void subtractScore(int scoreSubtracted) {
		score -= scoreSubtracted;
	}

	static boolean firstThread = true;

	public static Player getInstance() {
		if (firstInstance == null) {
			if (firstThread) {
				firstThread = false;
				try {
					Thread.currentThread();
					Thread.sleep(1000);
				} catch (InterruptedException e) {

					e.printStackTrace();
				}
			}
			synchronized (Player.class) {
				if (firstInstance == null) {
					firstInstance = new Player();
				}
			}
		}
		return firstInstance;
	}

	public boolean collision(Item item) {
		return collides.collide(this, item);
	}

	public Item shoot() {
		Iterator<Bullet> i = gun.createIterator();
		while (i.hasNext()) {
			return gun.Shoot();
		}
		return null;
	}

	@SuppressWarnings("static-access")
	@Override
	public void setIcon(Image newIcon) {
		this.icon = newIcon;

	}

	@SuppressWarnings("static-access")
	@Override
	public Image getIcon() {
		return this.icon;
	}

	@SuppressWarnings("static-access")
	public void setSprite(String imagePath) {
		try {
			this.sprite = ImageIO.read(new File(imagePath));
		} catch (IOException e) {
			e.printStackTrace();
		}
		updateSprites();
		icon = sprites[0].getScaledInstance(20, 20, Image.SCALE_DEFAULT);
	}

	@SuppressWarnings("static-access")
	private void updateSprites() {
		int rows = 4;
		int cols = 3;
		sprites = new BufferedImage[rows * cols];
		for (int i = 0; i < rows; i++) {
			for (int j = 0; j < cols; j++) {
				sprites[(i * cols) + j] = this.sprite.getSubimage(j * width, i * height, width, height);
			}
		}
	}

	@SuppressWarnings("static-access")
	public Image grapSprite(Direction direction) {
		int directionCode = direction.getCode();
		Image sprite;
		switch (directionCode) {
		// down (0 - 2)
		case 1:
			sprite = this.sprites[downCounter];
			if (downCounter == 2) {
				downCounter = 0;
			} else {
				downCounter++;
			}
			return sprite.getScaledInstance(20, 20, Image.SCALE_DEFAULT);

		// right (6 - 8)
		case 2:
			sprite = this.sprites[rightCounter];
			if (rightCounter == 8) {
				rightCounter = 6;
			} else {
				rightCounter++;
			}
			return sprite.getScaledInstance(20, 20, Image.SCALE_DEFAULT);

		// up (9 - 11)
		case 3:
			sprite = this.sprites[upCounter];
			if (upCounter == 11) {
				upCounter = 9;
			} else {
				upCounter++;
			}
			return sprite.getScaledInstance(20, 20, Image.SCALE_DEFAULT);

		// left (3 - 5)
		case 4:
			sprite = this.sprites[leftCounter];
			if (leftCounter == 5) {
				leftCounter = 3;
			} else {
				leftCounter++;
			}
			return sprite.getScaledInstance(20, 20, Image.SCALE_DEFAULT);
		}
		return new BufferedImage(0, 0, 0); // empty image.
	}

}
