package Model;

public abstract class Bomb extends Collectable{

	private int damage;

	public int getDamage() {
		return damage;
	}

	public void setDamage(int damage) {
		this.damage = damage;
	}
	
	public void setEffect(){
		effect = (-damage);
	}
}
