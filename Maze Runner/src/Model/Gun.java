package Model;

import java.util.ArrayList;
import java.util.Iterator;

public class Gun {

	private ArrayList<Bullet> bullets;

	public Gun() {
		bullets = new ArrayList<Bullet>();
		while (bullets.size() < 6) {
			bullets.add(new Bullet());
		}
	}

	public int getNumberOfBullets() {
		return bullets.size();
	}

	public void addBullets(int bulletsToAdd) {
		while (bullets.size() < 6 && bulletsToAdd > 0) {
			bullets.add(new Bullet());
			bulletsToAdd--;
		}
	}

	public void setNumberOfBullets(int NumberOfBullets) {
		bullets.clear();
		if (NumberOfBullets <= 6 && NumberOfBullets >= 0) {
			while (bullets.size() < NumberOfBullets) {
				bullets.add(new Bullet());
			}
		} else {
			System.out.println("wrong number of bullets");
		}
	}

	// public boolean haveBulets() {
	// if (bullets.isEmpty()) {
	// return false;
	// } else {
	// return true;
	// }
	// }

	public Bullet Shoot() {
		if (!bullets.isEmpty()) {
			return bullets.remove(bullets.size() - 1);
		} else {
			return null;
		}
	}

	public Iterator<Bullet> createIterator() {
		return bullets.iterator();
	}
}
