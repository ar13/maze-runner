package Model;

import java.awt.Point;
import java.util.ArrayList;
import java.util.Random;

import control.Direction;

public abstract class Monster extends Item {

	private Integer range;
	protected Point target;
	protected Integer speed;
	protected ArrayList<Point> currentPath;

	public Monster(int difficulty) {
		this.range = difficulty * 2;
		target = this.getPoistion();
		currentPath = new ArrayList<Point>();
	}

	public boolean canHunt(Point playerPosition) {
		return Math.sqrt((playerPosition.x - this.getPoistion().x) * (playerPosition.x - this.getPoistion().x)
				+ (playerPosition.y - this.getPoistion().y) * (playerPosition.y - this.getPoistion().y)) <= range;
	}

	public int getPathSize() {
		return currentPath.size();
	}

	public void setPath(ArrayList<Point> path) {
		currentPath = path;
	}

	public int getSpeed() {
		return this.speed;
	}

	public void initTarget() {
		target = this.getPoistion();
		currentPath.clear();
	}

	public boolean newTarget() {
		return target == this.getPoistion();
	}

	public Point PossibleTarget(int size) {
		if (target == this.getPoistion()) {
			Random randomizer = new Random();
			int x = randomizer.nextInt(size);
			int y = randomizer.nextInt(size);
			return new Point(x, y);
		}
		return target;
	}

	public void setTarget(Point target) {
		this.target = target;

	}

	protected Direction findDirection(Point currentPosition, Point nextPosition) {
		// up - x const, y decreases
		// down - x const, y increases.
		// right - x increases, y const.
		// left - x decreases, y const.

		if (currentPosition.x == nextPosition.x) {
			// up or down
			if (currentPosition.y > nextPosition.y) {
				return Direction.UP;
			} else {
				return Direction.DOWN;
			}
		} else if (currentPosition.y == nextPosition.y) {
			// left or right
			if (currentPosition.x > nextPosition.x) {
				return Direction.LEFT;
			} else {
				return Direction.RIGHT;
			}
		}
		return null;

	}

}
