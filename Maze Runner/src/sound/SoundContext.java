package sound;

public class SoundContext extends Sound {
	private Sound sound;

	public SoundContext(String filePath) {
		this.setFilePath(filePath);
	}

	public void setState(Sound sound) {
		this.sound = sound;
		
	}
   
	@Override
	public void doAction() {
		this.sound.doAction();

	}

}
