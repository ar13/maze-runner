package sound;

import sun.audio.AudioPlayer;

@SuppressWarnings("restriction")
public class StopSound  extends Sound{

	@Override
	public void doAction() {
		AudioPlayer.player.stop(audioStream);
		
	}

}
